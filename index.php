<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Usuários
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Novo</a>
          <a class="dropdown-item" href="#">Listar</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Adicionar Tarefa</a>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Grupos
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Novo</a>
          <a class="dropdown-item" href="#">Listar</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Adicionar Tarefa</a>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link disabled" href="#">Disabled</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 offset-md-3">
              <form id="formusuario">
                <div class="form-row">
                  <div class="form-group col-md-6 col-xs-12">
                    <label>Nome</label>
                    <input type="text" name="nome" class="form-control">
                  </div>
                  <div class="form-group col-md-6 col-xs-12">
                    <label>Login</label>
                    <input type="text" name="username" class="form-control">
                  </div>
                  <div class="form-group col-md-6 col-xs-12">
                    <label>Senha</label>
                    <input type="password" name="senha" class="form-control">
                  </div>
                  <div class="form-group col-md-6 col-xs-12">
                    <label>Tipo de usuário</label>
                    <select class="form-control" name="tipo_id">
                      <option value="1">Administrador</option>
                      <option value="2">Gerente</option>
                      <option value="3">Normal</option>
                    </select>
                  </div>
                  <input type="submit" class="btn btn-primary 
                          form-control" value="Salvar">
                </div>
              </form>
            </div>
        </div>
    </div>

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/todo.js"></script>
</body>

</html>