-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 15/03/2018 às 15:09
-- Versão do servidor: 5.7.21-0ubuntu0.16.04.1
-- Versão do PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `todoteste`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `grupos`
--

CREATE TABLE `grupos` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `data_criacao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tarefas`
--

CREATE TABLE `tarefas` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  `data_criacao` datetime DEFAULT CURRENT_TIMESTAMP,
  `data_finalizacao` datetime DEFAULT NULL,
  `prioridade` int(11) DEFAULT NULL COMMENT 'da menor para a maior: 0 a 10'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tipos`
--

CREATE TABLE `tipos` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tipos de usuários existentes no sistema: Administrador, Gerente, Normal.';

--
-- Fazendo dump de dados para tabela `tipos`
--

INSERT INTO `tipos` (`id`, `nome`) VALUES
(1, 'Administrador'),
(2, 'Gerente'),
(3, 'Normal');

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `avatar` varchar(45) DEFAULT NULL,
  `data_criacao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tipo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `username`, `senha`, `avatar`, `data_criacao`, `tipo_id`) VALUES
(13, 'maria', 'maria', 'senha', NULL, '2018-03-12 18:15:03', 2),
(14, 'juv', 'jub', 'senha', NULL, '2018-03-14 21:31:41', 2),
(15, 'juv', 'jub', 'senha', NULL, '2018-03-14 21:35:17', 2),
(16, 'juv', 'jub', 'senha', NULL, '2018-03-14 21:36:32', 2),
(17, 'juv', 'jub', 'senha', NULL, '2018-03-14 21:38:05', 2),
(18, 'juv', 'jub', 'senha', NULL, '2018-03-14 21:39:32', 2),
(19, 'juv', 'jub', 'senha', NULL, '2018-03-14 21:40:13', 2),
(20, 'juv', 'jub', 'asdad', NULL, '2018-03-14 21:41:21', 2),
(21, 'juv', 'jub', 'dasdd', NULL, '2018-03-14 21:42:51', 2),
(22, 'juv', 'jub', 'dasdd', NULL, '2018-03-14 21:44:17', 2),
(23, 'juv', 'jub', 'dasdd', NULL, '2018-03-14 21:46:28', 2),
(24, 'juv', 'jub', 'dasdd', NULL, '2018-03-14 21:47:18', 2),
(25, 'juv', 'jub', 'sdasd', NULL, '2018-03-14 21:58:07', 2),
(26, 'juv', 'jub', 'asdsad', NULL, '2018-03-14 22:00:57', 2),
(27, 'juv', 'jub', 'dsd', NULL, '2018-03-14 22:01:35', 2),
(28, 'as', 'asa', 'dasd', NULL, '2018-03-14 22:08:05', 1),
(29, 'as', 'asa', 'as', NULL, '2018-03-14 22:09:48', 1),
(30, 'as', 'asa', 'as', NULL, '2018-03-14 22:10:23', 1),
(31, 'casa', 'asa', 'as', NULL, '2018-03-14 22:10:55', 1),
(32, 'casa', 'asa', 'asdsad', NULL, '2018-03-14 22:13:00', 1),
(33, 'teste 2', 'asa', 'asdsad', NULL, '2018-03-14 22:13:11', 1),
(34, 'teste 2', 'asagh', 'asdsad', NULL, '2018-03-14 22:13:43', 1),
(35, 'teste 3', 'asagh', 'asdsad', NULL, '2018-03-14 22:14:43', 1),
(36, 'teste 5', 'asagh', 'asdsad', NULL, '2018-03-14 22:14:50', 1),
(37, 'teste 5', 'asagh', 'sadas', NULL, '2018-03-14 22:28:24', 1),
(38, 'ana', 'asagh', 'sadas', NULL, '2018-03-14 22:28:39', 1),
(39, 'ana', 'asagh', 'sdasd', NULL, '2018-03-14 22:30:56', 1),
(40, 'ana', 'asagh', 'adasda', NULL, '2018-03-14 22:34:22', 1),
(41, 'sdas', 'dasda', 'sadad', NULL, '2018-03-14 23:15:50', 1),
(42, 'sdas', 'dasda', 'sadad', NULL, '2018-03-14 23:18:38', 1),
(43, 'abcde', 'dasda', 'sadad', NULL, '2018-03-14 23:20:34', 1),
(44, 'abcdef', 'dasda', 'sadad', NULL, '2018-03-14 23:22:16', 1),
(45, 'ana', 'ana', 'ana', NULL, '2018-03-14 23:32:52', 2),
(46, 'ana', 'ana', 'ana', NULL, '2018-03-14 23:33:16', 2),
(47, 'ana', 'ana', 'ana', NULL, '2018-03-14 23:34:59', 1),
(48, 'ana', 'ana', 'ana', NULL, '2018-03-14 23:35:22', 1),
(49, 'ana', 'ana', 'ana', NULL, '2018-03-14 23:36:12', 1),
(50, 'ana', 'ana', 'ana', NULL, '2018-03-14 23:37:39', 1),
(51, 'ana', 'ana', 'ana', NULL, '2018-03-14 23:38:37', 2),
(52, 'ana', 'ana', 'ana', NULL, '2018-03-14 23:39:12', 3),
(53, 'maria', 'maria', 'ana', NULL, '2018-03-14 23:39:40', 1),
(54, 'dasd', 'dasd', 'dasd', NULL, '2018-03-14 23:43:46', 1),
(55, 'as', 'asd', 'asdas', NULL, '2018-03-15 00:01:26', 1),
(56, 'sada', 'dasd', 'xasdas', NULL, '2018-03-15 00:04:36', 1),
(57, 'df', 'ddf', 'sdfsf', NULL, '2018-03-15 11:01:17', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuarios_grupos`
--

CREATE TABLE `usuarios_grupos` (
  `usuarios_id` int(11) NOT NULL,
  `grupos_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuarios_tarefas`
--

CREATE TABLE `usuarios_tarefas` (
  `usuario_id` int(11) NOT NULL,
  `tarefa_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `tarefas`
--
ALTER TABLE `tarefas`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `tipos`
--
ALTER TABLE `tipos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_usuarios_tipos_idx` (`tipo_id`);

--
-- Índices de tabela `usuarios_grupos`
--
ALTER TABLE `usuarios_grupos`
  ADD PRIMARY KEY (`usuarios_id`,`grupos_id`),
  ADD KEY `fk_usuarios_has_grupos_grupos1_idx` (`grupos_id`),
  ADD KEY `fk_usuarios_has_grupos_usuarios1_idx` (`usuarios_id`);

--
-- Índices de tabela `usuarios_tarefas`
--
ALTER TABLE `usuarios_tarefas`
  ADD PRIMARY KEY (`usuario_id`,`tarefa_id`),
  ADD KEY `fk_usuarios_has_tarefas_tarefas1_idx` (`tarefa_id`),
  ADD KEY `fk_usuarios_has_tarefas_usuarios1_idx` (`usuario_id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `grupos`
--
ALTER TABLE `grupos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `tarefas`
--
ALTER TABLE `tarefas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `tipos`
--
ALTER TABLE `tipos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de tabela `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuarios_tipos` FOREIGN KEY (`tipo_id`) REFERENCES `tipos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `usuarios_grupos`
--
ALTER TABLE `usuarios_grupos`
  ADD CONSTRAINT `fk_usuarios_has_grupos_grupos1` FOREIGN KEY (`grupos_id`) REFERENCES `grupos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuarios_has_grupos_usuarios1` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `usuarios_tarefas`
--
ALTER TABLE `usuarios_tarefas`
  ADD CONSTRAINT `fk_usuarios_has_tarefas_tarefas1` FOREIGN KEY (`tarefa_id`) REFERENCES `tarefas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuarios_has_tarefas_usuarios1` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
