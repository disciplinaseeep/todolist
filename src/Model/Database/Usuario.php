<?php
namespace TodoList\Model\Database;

use TodoList\Model\Database\Conexao;

class Usuario{
    private $conexao = null;

    public function __construct(){
        $this->conexao = 
        (new Conexao())->getConexao();
    }

    /**
     * Cadastrar um novo usuario
     * @param string $nome
     * @return bool true se o usuario for salvo, 
     * false caso contrario
    */
    public function inserir(string $nome, 
    string $senha, string $username, 
    int $tipoId){

        $insert = $this->conexao->prepare(
            'INSERT INTO usuarios (nome, 
            username, senha, tipo_id)
            VALUES (:nome, :username, :senha, :tipo_id)'
        );

        $insert->bindValue(':nome', $nome);
        $insert->bindValue(':username', $username);
        $insert->bindValue(':senha', $senha);
        $insert->bindValue(':tipo_id', $tipoId);
        return $insert->execute();
    }
}