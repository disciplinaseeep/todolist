<?php
namespace TodoList\Model\Database;

class Conexao{
    private $host = '127.0.0.1';
    private $usuario = 'juvenildo';
    private $senha = 'senha';
    private $dbName = 'todoteste';

    /**
     * Essa função cria uma nova conexão com 
     * o banco de dados
     * 
     * @return PDO um objeto da classe PDO
    */
    public function getConexao(){
        try{
            //objeto de acesso a dados do php
            $conexao = new \PDO('mysql:host=' . $this->host . 
            ';dbname=' . $this->dbName, 
            $this->usuario, $this->senha);
        }catch(\PDOException $excecao){
            echo $excecao->getMessage();
            exit;
        }      
        return $conexao;
    }  
}

?>