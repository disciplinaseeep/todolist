<?php
namespace TodoList\Controller;
use TodoList\Model\Database\Usuario;

class UsuarioController{
    private $modelUsuario = null;

    public function __construct(){
        $this->modelUsuario = new Usuario();
    }

    public function salvar(){
        $this->modelUsuario->inserir($_POST['nome'],
        $_POST['senha'], $_POST['username'],
        $_POST['tipo_id']);
    }
}